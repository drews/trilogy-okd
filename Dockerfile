FROM rocker/shiny

MAINTAINER Ben Neely "ben.neely@duke.edu"

RUN apt-get update && apt-get install -y \
    libcurl4-openssl-dev \
    libxml2-dev

RUN R -e "install.packages(c('XML', 'scales', 'ggplot2'), repos='http://cran.rstudio.com/')"

COPY . /srv/shiny-server/


RUN chgrp -R 0 /etc/shiny-server && \
    chmod -R g=u /etc/shiny-server && \
    chgrp -R 0 /srv/shiny-server/ && \
    chmod -R g=u /srv/shiny-server/

RUN chmod g=u /etc/passwd
COPY fix-username.sh /fix-username.sh
COPY shiny-server.sh /usr/bin/shiny-server.sh
RUN chmod a+rx /usr/bin/shiny-server.sh

RUN chmod -R a+rwX /var/log/shiny-server
RUN chmod -R a+rwX /var/lib/shiny-server

EXPOSE 3838

